
import "dart:async";

import "package:postgresql/postgresql.dart" as postgresql;

typedef void MigrateFunction(postgresql.Connection dbConn);

Future<int> getDatabaseVersion(postgresql.Connection dbConn) async {
  final rows = dbConn.query(
    "SELECT 1 FROM information_schema.tables WHERE table_name = 'key_value_store';"
  );
  var version;
  if((await rows.length) == 0) {
    version = 0;
  } else {
    final rows = await dbConn.query("SELECT value FROM key_value_store WHERE key = 'schema_version';");
    version = int.parse((await rows.first).value, onError: (source) { return null; });
  }
  return version;
}

migrateDatabase(postgresql.Connection dbConn, MigrateFunction createLatest,
    List<MigrateFunction> migrateFunctions) async {
  final dbVersion = await getDatabaseVersion(dbConn);
  if(dbVersion == 0) {
    await createLatest(dbConn);
  } else {
    for(int i = dbVersion; i < migrateFunctions.length; ++i){
      await migrateFunctions[i](dbConn);
    }
  }
}


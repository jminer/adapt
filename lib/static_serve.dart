
import 'dart:io';

import "package:path/path.dart" as path;

const staticFileMimeTypes = const {
  "css": "text/css",
  "js": "application/javascript",
  "png": "image/png",
  "jpg": "image/jpeg",
  "woff": "font/woff",
  "woff2": "font/woff2",
};

staticFileGet(HttpRequest request, String filePath,
    [Map<String, String> additionalMimeTypes = const {}]) async {
  final ext = path.extension(filePath).substring(1);
  final mimeType = additionalMimeTypes[ext] ?? staticFileMimeTypes[ext];
  if(mimeType == null)
    throw new ArgumentError("filePath is an unsupported file type");
  request.response.headers.set(HttpHeaders.CONTENT_TYPE, "$mimeType; charset=utf-8");
  final localFile = new File("./build/web/$filePath");
  await localFile.openRead().pipe(request.response);
  await request.response.close();
}

library adapt;

import "dart:async";
import "dart:collection";
import "dart:convert";
import "dart:io";
import "dart:mirrors" as mirrors;

import "package:path/path.dart" as path;
import "package:html/parser.dart" as html_parser;
import "package:html/dom.dart";

Future<String> fileTemplateProvider(String templateFile, String parentFile) async {
  final parentDir = parentFile == null ? path.current : path.dirname(path.absolute(parentFile));
  final templatePath = path.join(parentDir, templateFile);
  try {
    return await new File(templatePath).readAsString();
  } on FileSystemException {
    return await new File("$templatePath.adapt").readAsString();
  }
}

// Loads a template given its file name. `parentFile` is the file name of the template
// causing this template to be loaded. `parentFile` may be null.
typedef Future<String> TemplateProvider(String templateFile, String parentFile);

// `currentFile` is the file that the specified node was loaded from.
_loadTemplateIncludes(node, String nodeFile, TemplateProvider templateProvider) async {
  for(var elem in node.querySelectorAll("adapt-include")) {
    final elemFile = elem.attributes["src"];
    final source = await templateProvider(elemFile, nodeFile);
    final fragment = html_parser.parseFragment(source, container: node.parent.localName);
    await _loadTemplateIncludes(fragment, elemFile, templateProvider);
    elem.replaceWith(fragment);
  }
}

_getValueFromContext(List contextStack, key) {
  for(var i = contextStack.length - 1; i >= 0; --i) {
    final context = contextStack[i];
    //var members = mirrors.reflect(context).type.instanceMembers;
    //var methods = methods.keys.map((k) => k.toString()).toList();
    final contextMirror = mirrors.reflect(context);
    try {
      return context[key] ?? "";
    } on NoSuchMethodError {
    }

    {
      final keySymbol = new Symbol(key);
      final resultMirror = contextMirror.getField(keySymbol);
      if(resultMirror.reflectee != null)
        return resultMirror.reflectee;
    }
  }
  return "";
}

// Splits off all code points from the beginning of the Text node to `splitStart` and returns
// them as a new Text node. The parent of `node` is not changed.
Text _splitTextNodeStart(Text node, int splitStart, int splitEnd) {
  final before = node.text.substring(0, splitStart);
  node.text = node.text.substring(splitEnd);
  return new Text(before);
}

// Splits off all code points from `splitEnd` to the end of the Text node and returns
// them as a new Text node. The parent of `node` is not changed.
Text _splitTextNodeEnd(Text node, int splitStart, int splitEnd) {
  final after = node.text.substring(splitEnd);
  node.text = node.text.substring(0, splitStart);
  return new Text(after);
}

_fillTemplate(Node parentNode, List contextStack) async {
  // I thought about looping in reverse (both of the following loops), but that would likely
  // result in more shifting because more nodes would be inserted.
  for(var nodeIndex = 0; nodeIndex < parentNode.nodes.length; ++nodeIndex) {
    final node = parentNode.nodes[nodeIndex];
    if(node.nodeType == Node.TEXT_NODE) {
      var textIndex = 0;
      while(true) {
        final tagStartIndex = node.text.indexOf("{{", textIndex);
        if(tagStartIndex == -1)
          break;
        final tagEndIndex = node.text.indexOf("}}", tagStartIndex + 2) + 2;

        final key = node.text.substring(tagStartIndex + 2, tagEndIndex - 2);
        var renderedKey = _getValueFromContext(contextStack, key);
        if(renderedKey is TemplateFragment) {
          renderedKey =
              await renderedKey.render(contextStack, (parentNode as Element).localName);
        }
        if(renderedKey is DocumentFragment) {
          final afterNode = _splitTextNodeEnd(node, tagStartIndex, tagEndIndex);
          parentNode.nodes.insertAll(nodeIndex + 1, [renderedKey, afterNode]);
          ++nodeIndex; // skip searching renderedKey
          break; // small optimization
          //  textIndex is now the end of the text node
        } else {
          assert(renderedKey is String);
          node.text = node.text.replaceRange(tagStartIndex, tagEndIndex, renderedKey);
          textIndex = tagStartIndex + renderedKey.length;
        }
      }
    } else if(node.nodeType == Node.ELEMENT_NODE) {
      final eachKey = node.attributes.remove("adapt-each");
      final ifKey = node.attributes.remove("adapt-if");
      if(eachKey != null) {
        final section = new DocumentFragment();
        if((node as Element).localName == "adapt-section") {
          // NodeList.addAll is so inefficient. It loops over the nodes in reverse, but then it
          // call remove() on each, which does an indexOf() on the node! So addAll would be O(n^2)
          // here. Plus, it gets six functions deep before it gets to List.removeAt()!
          final sectionNodes = node.nodes.sublist(0, node.nodes.length);
          node.nodes.clear();
          section.nodes.addAll(sectionNodes);
          node.remove();
        } else {
          // There has to be a DocumentFragment with just this node as a child because the fragment
          // is needed to pass to _fillTemplate(). _fillTemplate() only fills the attributes of
          // children, not of the node passed to it.
          section.nodes.add(node);
        }
        --nodeIndex; // This node was just removed.

        final value = _getValueFromContext(contextStack, eachKey);
        if(value is Iterable) {
          final accumFragment = new DocumentFragment();
          for(var item in value) {
            contextStack.add(item);
            final clonedSection = section.clone(true);
            await  _fillTemplate(clonedSection, contextStack);
            contextStack.removeLast();
            accumFragment.append(clonedSection);
          }
          parentNode.nodes.insert(nodeIndex + 1, accumFragment);
          nodeIndex += accumFragment.nodes.length;
        } else if(value is Function) {
          DocumentFragment mappedSection = value(section);
          parentNode.nodes.insert(nodeIndex + 1, mappedSection);
          nodeIndex += mappedSection.nodes.length;
        }
      } else if(ifKey != null) {

      } else {
        if((node as Element).localName == "adapt-section") {
          throw new FormatException(
              "adapt-section tags are only supported with adapt-each or adapt-if attributes");
        }

        final attributeNames = node.attributes.keys;
        for(var name in attributeNames) {
          // TODO: do replace without regex
          final newValue = node.attributes[name].replaceAllMapped(new RegExp(r"\{\{(.*?)\}\}"), (match) {
            return _getValueFromContext(contextStack, match.group(1));
          });
          node.attributes[name] = newValue;
        }
      }
    }

    if(node.nodes.length != 0)
      await _fillTemplate(node, contextStack);
  }
}

class Template {
  String _file;
  TemplateProvider _templateProvider;
  String _source;
  Document _document;

  Template.fromSource(String this._source,
      [TemplateProvider this._templateProvider = fileTemplateProvider]) {
    if(_source == null)
      throw new ArgumentError.notNull("source");
  }

  Template.fromFile(String this._file,
      [TemplateProvider this._templateProvider = fileTemplateProvider]) {
    if(_file == null)
      throw new ArgumentError.notNull("file");
  }

  Future<Document> render(data) async {
    if(_document == null) {
      if(_source == null)
        _source = await _templateProvider(_file, null);
      final document = html_parser.parse(_source);
      //print(document.outerHtml);
      await _loadTemplateIncludes(document, _file, _templateProvider);
      _document = document;
    }

    final doc = _document.clone(true);
    await _fillTemplate(doc, [data]);
    return doc;
  }

  optimize(Document document) {
    // remove comments and id adapt-*
  }
}

class TemplateFragment {
  String _file;
  TemplateProvider _templateProvider;
  String _source;
  HashMap<String, DocumentFragment> _fragments;

  TemplateFragment.fromSource(String this._source,
      [TemplateProvider this._templateProvider = fileTemplateProvider]) {
    if(_source == null)
      throw new ArgumentError.notNull("source");
    _fragments = new HashMap<String, DocumentFragment>();
  }

  TemplateFragment.fromFile(String this._file,
      [TemplateProvider this._templateProvider = fileTemplateProvider]) {
    if(_file == null)
      throw new ArgumentError.notNull("file");
    _fragments = new HashMap<String, DocumentFragment>();
  }

  Future<DocumentFragment> render(List contextStack, container) async {
    DocumentFragment fragment;
    // I cannot use HashMap.putIfAbsent() because it takes a function that is not async.
    if(_fragments.containsKey(container)) {
      fragment = _fragments[container];
    } else {
      if(_source == null)
        _source = await _templateProvider(_file, null);
      final tmp = html_parser.parseFragment(_source, container: container);
      await _loadTemplateIncludes(tmp, _file, _templateProvider);
      fragment = tmp;
    }

    fragment = fragment.clone(true);
    await _fillTemplate(fragment, contextStack);
    return fragment;
  }
}


class _SingleObjectBuffer<T> implements Sink<T> {
  T _data;
  add(T data) {
    _data = data;
  }
  close() {}
  get data => _data;
}

const JsonDecoder _jsonDecoder = const JsonDecoder();

decodeRequestJson(HttpRequest request) async {
  final objBuffer = new _SingleObjectBuffer();
  final sink = _jsonDecoder.startChunkedConversion(objBuffer).asUtf8Sink(false);
  await for(var chunk in request) {
    sink.add(chunk);
  }
  sink.close();
  return objBuffer.data;
}


final slugSepRegex = new RegExp(r"[^a-z0-9_\-\.]+");

String getSlug(String text) {
  return text.trim().toLowerCase().replaceAll("'", "").replaceAll(slugSepRegex, "-");
}

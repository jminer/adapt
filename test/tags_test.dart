
import "package:adapt/adapt.dart" as adapt;

import "package:test/test.dart";

final leadingSpaces = new RegExp(r"^ +", multiLine: true);

main() {
  test("Variable tags with string values", () async {
    var template = new adapt.Template.fromSource("""
<!DOCTYPE html>
<html>
  <body>
    <h1>{{title}}</h1>
    <p>Hello, {{name}}, and welcome!</p>
  </body>
</html>""");
    var doc = await template.render({
      "title": "The Title Goes Here",
      "name": "Ken"
    });
    expect(doc.outerHtml.replaceAll(leadingSpaces, ""), equals("""
<!DOCTYPE html><html><head></head><body>
    <h1>The Title Goes Here</h1>
    <p>Hello, Ken, and welcome!</p>

    </body></html>""".replaceAll(leadingSpaces, "")));
  });


  test("Variable tags with TemplateFragment and DocumentFragment values", () async {
    var templateFrag = new adapt.TemplateFragment.fromSource("""
<strong>{{name1}}</strong>""");
    var docFrag = await templateFrag.render([{ "name1": "Mike" }], "p");
    var template = new adapt.Template.fromSource("""
<!DOCTYPE html>
<html>
  <body>
    <h1>{{title}}</h1>
    <p>Hello, {{nameTemplate}} and {{name2}}, and welcome!</p>
  </body>
</html>""");
    var doc = await template.render({
      "title": "The Title Goes Here",
      "nameTemplate": templateFrag,
      "name1": "Ken",
      "name2": docFrag
    });
    expect(doc.outerHtml.replaceAll(leadingSpaces, ""), equals("""
<!DOCTYPE html><html><head></head><body>
    <h1>The Title Goes Here</h1>
    <p>Hello, <strong>Ken</strong> and <strong>Mike</strong>, and welcome!</p>

    </body></html>""".replaceAll(leadingSpaces, "")));
  });


  test("Section element using adapt-each", () async {
    var template = new adapt.Template.fromSource("""
<!DOCTYPE html>
<html>
  <body>
    <h1>{{title}}</h1>
    <dl>
    <adapt-section adapt-each=members>
      <dt>Name: {{name}}, male</dt>
      <dd>DOB: {{birthDate}}</dd>
    </adapt-section>
    </dl>
  </body>
</html>""");
    var doc = await template.render({
      "title": "The Title Goes Here",
      "members": [
        { "name": "Gary", "birthDate": "1972-03-05" },
        { "name": "Chuck", "birthDate": "1963-02-22" },
        { "name": "Tony", "birthDate": "1985-05-07" }
      ]
    });
    expect(doc.outerHtml.replaceAll(leadingSpaces, ""), equals("""
<!DOCTYPE html><html><head></head><body>
    <h1>The Title Goes Here</h1>
    <dl>

      <dt>Name: Gary, male</dt>
      <dd>DOB: 1972-03-05</dd>

      <dt>Name: Chuck, male</dt>
      <dd>DOB: 1963-02-22</dd>

      <dt>Name: Tony, male</dt>
      <dd>DOB: 1985-05-07</dd>

    </dl>

    </body></html>""".replaceAll(leadingSpaces, "")));
  });


  test("Variable tag in attribute", () async {
    var template = new adapt.Template.fromSource("""
<!DOCTYPE html>
<html>
  <body>
    <h1>{{title}}</h1>
    <a href="{{linkUrl}}">{{linkTitle}}</a>
  </body>
</html>""");
    var doc = await template.render({
      "title": "Harmony",
      "linkUrl": "https://www.rust-lang.org",
      "linkTitle": "Rust"
    });
    expect(doc.outerHtml.replaceAll(leadingSpaces, ""), equals("""
<!DOCTYPE html><html><head></head><body>
    <h1>Harmony</h1>
    <a href="https://www.rust-lang.org">Rust</a>

  </body></html>""".replaceAll(leadingSpaces, "")));
  });


  test("adapt-each attribute", () async {
    var template = new adapt.Template.fromSource("""
<!DOCTYPE html>
<html>
  <body>
    <h1>{{title}}</h1>
    <a adapt-each=bookmarks href="{{url}}">{{title}}</a>
  </body>
</html>""");
    var doc = await template.render({
      "title": "Harmony",
      "bookmarks": [
        { "title": "Microsoft", "url": "https://www.microsoft.com" },
        { "title": "Rust", "url": "https://www.rust-lang.org" }
      ]
    });
    expect(doc.outerHtml.replaceAll(leadingSpaces, ""), equals("""
<!DOCTYPE html><html><head></head><body>
    <h1>Harmony</h1>
    <a href="https://www.microsoft.com">Microsoft</a><a href="https://www.rust-lang.org">Rust</a>

  </body></html>""".replaceAll(leadingSpaces, "")));
  });
}

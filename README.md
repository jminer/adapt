

## Template syntax

Adapt's template syntax is simple and almost logicless, in many ways similar to [Mustache](https://mustache.github.io/).

### Substitution

Text can be inserted by using a variable name wrapped in two sets of braces:

~~~~ html
<h2>Logged in as {{userName}}</h2>
~~~~

The template is first parsed as HTML, then element and attribute text is searched for any replacements that need to be done. Therefore, unlike Mustache, tag names cannot be determined by a substitution, nor can whether attributes are added. Substitutions must only be in attribute values or inside elements. Both of the following are invalid:

~~~~ html
<!-- invalid due to `headerLevel` being in a tag name -->
<h{{headerLevel}}>{{articleTitle}}</h{{headerLevel}}>
<!-- invalid due to `checkedAttribute` -->
<input type=checkbox {{checkedAttribute}} value=subscribe>
~~~~

### if

~~~~ html
<div adapt-if=showSidebar>
    <h3>Events</h3>
    ...
</div>
~~~~

### each

~~~~ html
<div adapt-if=showSidebar>
    <h3>Events</h3>
    <ul>
        <li adapt-each=events>{{description}}</li>
    </ul>
</div>
~~~~

~~~~ html
<dl>
  <adapt-section adapt-each=words>
  <dt>{{text}}</dt>
  <dd>{{definition}}</dd>
  </adapt-section>
</dl>
~~~~

### include

Includes the specified template relative to the current template.

~~~~ html
<adapt-include src="menuTemplate.html">
~~~~

### id

~~~~ html
<div adapt-id=faq-container>
</div>
~~~~
